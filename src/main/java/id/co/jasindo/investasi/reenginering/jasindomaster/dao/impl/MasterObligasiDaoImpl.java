package id.co.jasindo.investasi.reenginering.jasindomaster.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import id.co.jasindo.investasi.reenginering.jasindomaster.dao.MasterObligasiDao;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterObligasi;
import id.co.jasindo.investasi.reenginering.jasindomaster.service.MasterObligasiRepo;

@Repository
public class MasterObligasiDaoImpl implements MasterObligasiDao{

	@Autowired
	private MasterObligasiRepo masterObligasiRepo;
	
	@Override
	public List<InvReMasterObligasi> getAll() {
		List<InvReMasterObligasi> masterObligasi = new ArrayList<InvReMasterObligasi>();
		Iterator<InvReMasterObligasi> iterator = masterObligasiRepo.findAll().iterator();
		while (iterator.hasNext()) {
			masterObligasi.add(iterator.next());
		}
		return masterObligasi;
	}


}
