package id.co.jasindo.investasi.reenginering.jasindomaster.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import id.co.jasindo.investasi.reenginering.jasindomaster.dao.MasterBankDao;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.Bank;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterBank;
import id.co.jasindo.investasi.reenginering.jasindomaster.service.BankRepo2;
import id.co.jasindo.investasi.reenginering.jasindomaster.service.InvrMasterBankRepo;

@Component
public class MasterBankDaoImpl implements MasterBankDao {
	
	@Autowired
	private InvrMasterBankRepo invrMasterBankRepo;
	
	@Autowired
	private BankRepo2 bankRepo2;
	
	
	@Override
	public List<InvReMasterBank> getAllDataBankInvestasi() {
		List<InvReMasterBank> invReMasterBanks = new ArrayList<InvReMasterBank>();
		Iterator<InvReMasterBank> iterator = invrMasterBankRepo.findAll().iterator();
		while (iterator.hasNext()) {
			invReMasterBanks.add(iterator.next());
		}
		return invReMasterBanks;
	}

	@Override
	public List<Bank> getAllDataBank() {
		List<Bank> Banks = new ArrayList<Bank>();
		Iterator<Bank> iterator = bankRepo2.findAll().iterator();
		while (iterator.hasNext()) {
			Banks.add(iterator.next());
		}
		return Banks;
	}

}
