/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.co.jasindo.investasi.reenginering.jasindomaster.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author dinoprasetyo
 */
@Entity
@Table(name = "INVR_M_OBLIGASI")
public class InvReMasterObligasi implements Serializable {
	private static final long serialVersionUID = 7081415004915157904L;

	@Id
	@Basic(optional = false)
	@Column(name = "KODE_OBLIGASI")
	private String kodeObligasi;

	@Column(name = "NAMA_OBLIGASI")
	private String namaObligasi;

	@Column(name = "KODE_MATA_UANG")
	private String kodeMataUang;

	@Column(name = "CREATED_BY")
	private BigInteger createdBy;

	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "UPDATED_BY")
	private BigInteger updatedBy;

	@Column(name = "UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	
	@Column(name = "RATING")
	private String rating;

	@Column(name = "RATING_DATE")
	private Date ratingDate;

	@JoinColumn(name = "KODE_EMITEN", referencedColumnName = "KODE_EMITEN", insertable=false, updatable=false)
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	private InvReMasterEmiten emiten;
	
	@Column(name = "KODE_EMITEN")
	private String kodeEmiten;
	
	@Column(name = "KODE_JENIS_OBLIGASI")
	private String obligasiJenis;
	
	@Column(name = "TGL_JATUH_TEMPO")
	@Temporal(TemporalType.DATE)
	private Date tglJatuhTempo;
	
	@Column(name = "TERMIN")
	private Short termin;
	
	@Column(name = "TGL_BAYAR_KUPON")
	private Integer tglBayarKupon;
	
	@Column(name = "PCT_KUPON")
	private BigDecimal pctKupon = BigDecimal.ZERO;
	
	@Column(name = "PCT_PAJAK")
	private BigDecimal pctPajak = BigDecimal.ZERO;
	
	@Column(name = "KODE_KATEGORI_INSTRUMEN")
	private String kodeKategoriInstrumen;
	
	@Column(name = "KODE_SEKURITAS")
	private String sekuritasCode;
	

	public String getSekuritasCode() {
		return sekuritasCode;
	}

	public void setSekuritasCode(String sekuritasCode) {
		this.sekuritasCode = sekuritasCode;
	}

	public String getKodeKategoriInstrumen() {
		return kodeKategoriInstrumen;
	}

	public void setKodeKategoriInstrumen(String kodeKategoriInstrumen) {
		this.kodeKategoriInstrumen = kodeKategoriInstrumen;
	}

	public Short getTermin() {
		return termin;
	}

	public void setTermin(Short termin) {
		this.termin = termin;
	}

	public Integer getTglBayarKupon() {
		return tglBayarKupon;
	}

	public void setTglBayarKupon(Integer tglBayarKupon) {
		this.tglBayarKupon = tglBayarKupon;
	}

	public BigDecimal getPctKupon() {
		return pctKupon;
	}

	public void setPctKupon(BigDecimal pctKupon) {
		this.pctKupon = pctKupon;
	}

	public BigDecimal getPctPajak() {
		return pctPajak;
	}

	public void setPctPajak(BigDecimal pctPajak) {
		this.pctPajak = pctPajak;
	}

	public Date getTglJatuhTempo() {
		return tglJatuhTempo;
	}

	public void setTglJatuhTempo(Date tglJatuhTempo) {
		this.tglJatuhTempo = tglJatuhTempo;
	}

	public String getObligasiJenis() {
		return obligasiJenis;
	}

	public void setObligasiJenis(String obligasiJenis) {
		this.obligasiJenis = obligasiJenis;
	}

	public InvReMasterObligasi() {
	}

	public InvReMasterObligasi(String kodeObligasi) {
		this.kodeObligasi = kodeObligasi;
	}

	public String getKodeObligasi() {
		return kodeObligasi;
	}

	public void setKodeObligasi(String kodeObligasi) {
		this.kodeObligasi = kodeObligasi;
	}

	public String getNamaObligasi() {
		return namaObligasi;
	}

	public void setNamaObligasi(String namaObligasi) {
		this.namaObligasi = namaObligasi;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public String getKodeMataUang() {
		return kodeMataUang;
	}

	public void setKodeMataUang(String kodeMataUang) {
		this.kodeMataUang = kodeMataUang;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigInteger getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public InvReMasterEmiten getEmiten() {
		return emiten;
	}

	public void setEmiten(InvReMasterEmiten emiten) {
		this.emiten = emiten;
	}
	

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Date getRatingDate()
	{
		return ratingDate;
	}

	public void setRatingDate(Date ratingDate)
	{
		this.ratingDate = ratingDate;
	}

	public String getKodeEmiten() {
		return kodeEmiten;
	}

	public void setKodeEmiten(String kodeEmiten) {
		this.kodeEmiten = kodeEmiten;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (kodeObligasi != null ? kodeObligasi.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof InvReMasterObligasi)) {
			return false;
		}
		InvReMasterObligasi other = (InvReMasterObligasi) object;
		if ((this.kodeObligasi == null && other.kodeObligasi != null)
				|| (this.kodeObligasi != null && !this.kodeObligasi.equals(other.kodeObligasi))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "id.co.jasindo.investasi.reenginering.model.InvReMasterObligasi[ kodeObligasi=" + kodeObligasi + " ]";
	}

}
