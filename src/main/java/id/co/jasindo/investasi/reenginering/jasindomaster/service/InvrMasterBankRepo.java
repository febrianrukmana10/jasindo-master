package id.co.jasindo.investasi.reenginering.jasindomaster.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterBank;

@Repository
public interface InvrMasterBankRepo extends CrudRepository<InvReMasterBank, Long>{

}
