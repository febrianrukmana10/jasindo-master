/*
* $Id: Bank.java 11876 2010-10-05 07:24:59Z dprasetyo $
* =====================================================================================================================
* Copyright (c) 2009 - BaliCamp - The Software Development Camp.
*/
package id.co.jasindo.investasi.reenginering.jasindomaster.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @author <a href="mailto:dgultom@balicamp.com">Daniel Leonardo Niko Gultom</a>
 * @version $Revision: 11876 $
 */
@Entity
@Table(name = "M_BANK")
public class Bank implements Serializable {

    private static final long serialVersionUID = 2747122207941509227L;

    private Long kodeBank;
    private String nama;
    private Short tipeBank;
    private String alamat;
    private String groupBank;
    private Long  kodeCabang;
    private String kodeAndName;
    private Boolean isAktif= Boolean.TRUE;

    public Bank() {}

    @Id
    @Column(name = "KODE_BANK")
    public Long getKodeBank() {
        return kodeBank;
    }

    public void setKodeBank(Long kodeBank) {
        this.kodeBank = kodeBank;
    }
    
    @Column(name = "NAMA_BANK", length = 50, nullable = false)
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Column(name = "TIPE_BANK")
    public Short getTipeBank() {
        return tipeBank;
    }

    public void setTipeBank(Short tipeBank) {
        this.tipeBank = tipeBank;
    }

    @Column(name = "ALAMAT", length = 50)
    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Column(name = "GROUP_BANK", length = 15)
    public String getGroupBank() {
        return groupBank;
    }

    public void setGroupBank(String groupBank) {
        this.groupBank = groupBank;
    }

    @Column(name = "IS_AKTIF")
    public Boolean getIsAktif()
    {
        return isAktif;
    }

    public void setIsAktif(Boolean aktif)
    {
        isAktif = aktif;
    }

    @Transient
    public String getKodeAndName() {
        return getKodeBank() + " - " + getNama();
    }

    public void setKodeAndName(String kodeAndName) {
        this.kodeAndName = kodeAndName;
    }

	@Column(name = "KODE_CABANG")
	public Long getKodeCabang()
	{
		return kodeCabang;
	}

	public void setKodeCabang(Long kodeCabang)
	{
		this.kodeCabang = kodeCabang;
	}

	@Override
	public String toString() {
		return "Bank [kodeBank=" + kodeBank + ", nama=" + nama + ", tipeBank=" + tipeBank + ", alamat=" + alamat
				+ ", groupBank=" + groupBank + ", kodeCabang=" + kodeCabang + ", kodeAndName=" + kodeAndName
				+ ", isAktif=" + isAktif + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alamat == null) ? 0 : alamat.hashCode());
		result = prime * result + ((groupBank == null) ? 0 : groupBank.hashCode());
		result = prime * result + ((isAktif == null) ? 0 : isAktif.hashCode());
		result = prime * result + ((kodeAndName == null) ? 0 : kodeAndName.hashCode());
		result = prime * result + ((kodeBank == null) ? 0 : kodeBank.hashCode());
		result = prime * result + ((kodeCabang == null) ? 0 : kodeCabang.hashCode());
		result = prime * result + ((nama == null) ? 0 : nama.hashCode());
		result = prime * result + ((tipeBank == null) ? 0 : tipeBank.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bank other = (Bank) obj;
		if (alamat == null) {
			if (other.alamat != null)
				return false;
		} else if (!alamat.equals(other.alamat))
			return false;
		if (groupBank == null) {
			if (other.groupBank != null)
				return false;
		} else if (!groupBank.equals(other.groupBank))
			return false;
		if (isAktif == null) {
			if (other.isAktif != null)
				return false;
		} else if (!isAktif.equals(other.isAktif))
			return false;
		if (kodeAndName == null) {
			if (other.kodeAndName != null)
				return false;
		} else if (!kodeAndName.equals(other.kodeAndName))
			return false;
		if (kodeBank == null) {
			if (other.kodeBank != null)
				return false;
		} else if (!kodeBank.equals(other.kodeBank))
			return false;
		if (kodeCabang == null) {
			if (other.kodeCabang != null)
				return false;
		} else if (!kodeCabang.equals(other.kodeCabang))
			return false;
		if (nama == null) {
			if (other.nama != null)
				return false;
		} else if (!nama.equals(other.nama))
			return false;
		if (tipeBank == null) {
			if (other.tipeBank != null)
				return false;
		} else if (!tipeBank.equals(other.tipeBank))
			return false;
		return true;
	}
}
