package id.co.jasindo.investasi.reenginering.jasindomaster.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.jasindo.investasi.reenginering.jasindomaster.dao.MasterObligasiDao;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterObligasi;

@RestController
@RequestMapping("/obligasi")
public class MasterObligasiController {

	@Autowired
	private MasterObligasiDao masterObligasiDao;
	@RequestMapping(value="/all", method = RequestMethod.GET)
    public List<InvReMasterObligasi> getObligasiAll(){
		System.out.println("master");
        return (List<InvReMasterObligasi>) masterObligasiDao.getAll();
    }
}
