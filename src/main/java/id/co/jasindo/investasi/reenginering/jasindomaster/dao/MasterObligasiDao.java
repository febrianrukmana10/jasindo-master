package id.co.jasindo.investasi.reenginering.jasindomaster.dao;

import java.util.List;

import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterObligasi;

public interface MasterObligasiDao {
	List<InvReMasterObligasi> getAll();
}
