/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.co.jasindo.investasi.reenginering.jasindomaster.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author alpinthor
 */
@Entity
@Table(name = "INVR_M_BANK")
public class InvReMasterBank implements Serializable{
	private static final long serialVersionUID = 5449172523921784277L;

	@Id
	@Basic(optional = false)
	@Column(name = "KODE_BANK")
	private Long kodeBank;

	@Basic(optional = false)
	@Column(name = "NAMA")
	private String nama;

	@Column(name = "ALAMAT")
	private String alamat;

	@Column(name = "KODE_CABANG")
	private String kodeCabang;

	@Column(name = "KODE_GROUP_BANK")
	private String kodeGroupBank;

	@Column(name = "KODE_REKANAN")
	private Long kodeRekanan;

	@Column(name = "CREATED_BY")
	private BigInteger createdBy;

	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "UPDATED_BY")
	private BigInteger updatedBy;

	@Column(name = "UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	@Column(name = "IS_OWN_ACCOUNT")
	private boolean isOwnAccount;

	@Column(name = "IS_BUMN")
	private boolean isBumn;

	@Basic(optional = false)
	@Column(name = "IS_BANK_DEPOSITO")
	private boolean isDeposito;

	@Column(name = "NAMA_CABANG_BANK")
	private String namaCabangBank;

	@Column(name = "REF_KODE_BANK_JASINDO")
	private Long refKodeBankJasindo;
	
	public InvReMasterBank() {
	}

	public InvReMasterBank(Long kodeBank) {
		this.kodeBank = kodeBank;
	}

	public Long getKodeBank() {
		return kodeBank;
	}

	public void setKodeBank(Long kodeBank) {
		this.kodeBank = kodeBank;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getKodeGroupBank() {
		return kodeGroupBank;
	}

	public void setKodeGroupBank(String kodeGroupBank) {
		this.kodeGroupBank = kodeGroupBank;
	}

	public Long getKodeRekanan() {
		return kodeRekanan;
	}

	public void setKodeRekanan(Long kodeRekanan) {
		this.kodeRekanan = kodeRekanan;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigInteger getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean isOwnAccount() {
		return isOwnAccount;
	}

	public void setOwnAccount(boolean isOwnAccount) {
		this.isOwnAccount = isOwnAccount;
	}

	public boolean isBumn() {
		return isBumn;
	}

	public void setBumn(boolean isBumn) {
		this.isBumn = isBumn;
	}

	public boolean isDeposito() {
		return isDeposito;
	}

	public void setDeposito(boolean isDeposito) {
		this.isDeposito = isDeposito;
	}

	public String getNamaCabangBank() {
		return namaCabangBank;
	}

	public void setNamaCabangBank(String namaCabangBank) {
		this.namaCabangBank = namaCabangBank;
	}

	public Long getRefKodeBankJasindo() {
		return refKodeBankJasindo;
	}

	public void setRefKodeBankJasindo(Long refKodeBankJasindo) {
		this.refKodeBankJasindo = refKodeBankJasindo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alamat == null) ? 0 : alamat.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + (isBumn ? 1231 : 1237);
		result = prime * result + (isDeposito ? 1231 : 1237);
		result = prime * result + (isOwnAccount ? 1231 : 1237);
		result = prime * result + ((kodeBank == null) ? 0 : kodeBank.hashCode());
		result = prime * result + ((kodeCabang == null) ? 0 : kodeCabang.hashCode());
		result = prime * result + ((kodeGroupBank == null) ? 0 : kodeGroupBank.hashCode());
		result = prime * result + ((kodeRekanan == null) ? 0 : kodeRekanan.hashCode());
		result = prime * result + ((nama == null) ? 0 : nama.hashCode());
		result = prime * result + ((namaCabangBank == null) ? 0 : namaCabangBank.hashCode());
		result = prime * result + ((refKodeBankJasindo == null) ? 0 : refKodeBankJasindo.hashCode());
		result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvReMasterBank other = (InvReMasterBank) obj;
		if (alamat == null) {
			if (other.alamat != null)
				return false;
		} else if (!alamat.equals(other.alamat))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (isBumn != other.isBumn)
			return false;
		if (isDeposito != other.isDeposito)
			return false;
		if (isOwnAccount != other.isOwnAccount)
			return false;
		if (kodeBank == null) {
			if (other.kodeBank != null)
				return false;
		} else if (!kodeBank.equals(other.kodeBank))
			return false;
		if (kodeCabang == null) {
			if (other.kodeCabang != null)
				return false;
		} else if (!kodeCabang.equals(other.kodeCabang))
			return false;
		if (kodeGroupBank == null) {
			if (other.kodeGroupBank != null)
				return false;
		} else if (!kodeGroupBank.equals(other.kodeGroupBank))
			return false;
		if (kodeRekanan == null) {
			if (other.kodeRekanan != null)
				return false;
		} else if (!kodeRekanan.equals(other.kodeRekanan))
			return false;
		if (nama == null) {
			if (other.nama != null)
				return false;
		} else if (!nama.equals(other.nama))
			return false;
		if (namaCabangBank == null) {
			if (other.namaCabangBank != null)
				return false;
		} else if (!namaCabangBank.equals(other.namaCabangBank))
			return false;
		if (refKodeBankJasindo == null) {
			if (other.refKodeBankJasindo != null)
				return false;
		} else if (!refKodeBankJasindo.equals(other.refKodeBankJasindo))
			return false;
		if (updatedBy == null) {
			if (other.updatedBy != null)
				return false;
		} else if (!updatedBy.equals(other.updatedBy))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InvReMasterBank [kodeBank=" + kodeBank + ", nama=" + nama + ", alamat=" + alamat + ", kodeCabang="
				+ kodeCabang + ", kodeGroupBank=" + kodeGroupBank + ", kodeRekanan=" + kodeRekanan + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy + ", updatedDate="
				+ updatedDate + ", isOwnAccount=" + isOwnAccount + ", isBumn=" + isBumn + ", isDeposito=" + isDeposito
				+ ", namaCabangBank=" + namaCabangBank + ", refKodeBankJasindo=" + refKodeBankJasindo + "]";
	}
	
	

}
