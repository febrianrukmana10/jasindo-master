package id.co.jasindo.investasi.reenginering.jasindomaster.dao;

import java.util.List;

import id.co.jasindo.investasi.reenginering.jasindomaster.model.Bank;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterBank;

/*@Repository
public interface MasterBankDao extends CrudRepository<InvReMasterBank, Long> {
    
}*/
public interface MasterBankDao {
	List<InvReMasterBank> getAllDataBankInvestasi();
	List<Bank> getAllDataBank();
}