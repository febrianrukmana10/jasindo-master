

package id.co.jasindo.investasi.reenginering.jasindomaster.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author dinoprasetyo
 */
@Entity
@Table(name = "INVR_M_EMITEN")
public class InvReMasterEmiten implements Serializable
{

	private static final long serialVersionUID = -3958410647957921580L;

	@Id
	@Basic(optional = false)
	@Column(name = "KODE_EMITEN")
	private String kodeEmiten;

	@Basic(optional = false)
	@Column(name = "NAMA_EMITEN")
	private String namaEmiten;

	@Column(name = "IS_EMITEN_SAHAM")
	private Boolean isEmitenSaham;

	@Column(name = "IS_EMITEN_OBLIGASI")
	private Boolean isEmitenObligasi;

	@Column(name = "CREATED_BY")
	private BigInteger createdBy;

	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "UPDATED_BY")
	private BigInteger updatedBy;

	@Column(name = "UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;

	public InvReMasterEmiten()
	{
	}

	public InvReMasterEmiten(String kodeEmiten)
	{
		this.kodeEmiten = kodeEmiten;
	}

	public InvReMasterEmiten(String kodeEmiten, String namaEmiten)
	{
		this.kodeEmiten = kodeEmiten;
		this.namaEmiten = namaEmiten;
	}

	public String getKodeEmiten()
	{
		return kodeEmiten;
	}

	public void setKodeEmiten(String kodeEmiten)
	{
		this.kodeEmiten = kodeEmiten;
	}

	public String getNamaEmiten()
	{
		return namaEmiten;
	}

	public void setNamaEmiten(String namaEmiten)
	{
		this.namaEmiten = namaEmiten;
	}

	public Boolean getEmitenSaham()
	{
		return isEmitenSaham;
	}

	public void setEmitenSaham(Boolean emitenSaham)
	{
		isEmitenSaham = emitenSaham;
	}

	public Boolean getEmitenObligasi()
	{
		return isEmitenObligasi;
	}

	public void setEmitenObligasi(Boolean emitenObligasi)
	{
		isEmitenObligasi = emitenObligasi;
	}

	public BigInteger getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public BigInteger getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (kodeEmiten != null ? kodeEmiten.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof InvReMasterEmiten))
		{
			return false;
		}
		InvReMasterEmiten other = (InvReMasterEmiten) object;
		if ((this.kodeEmiten == null && other.kodeEmiten != null) || (this.kodeEmiten != null && !this.kodeEmiten.equals(other.kodeEmiten)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "id.co.jasindo.investasi.reenginering.model.InvReMasterEmiten[ kodeEmiten=" + kodeEmiten + " ]";
	}

}
