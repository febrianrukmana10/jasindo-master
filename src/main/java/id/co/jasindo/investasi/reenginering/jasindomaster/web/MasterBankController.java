package id.co.jasindo.investasi.reenginering.jasindomaster.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.jasindo.investasi.reenginering.jasindomaster.dao.MasterBankDao;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.Bank;
import id.co.jasindo.investasi.reenginering.jasindomaster.model.InvReMasterBank;

@RestController
@RequestMapping("/masterBankController")
public class MasterBankController {
	
	@Autowired 
	private MasterBankDao masterBankDao;
	
	@RequestMapping(value="/masterBankInvestasi", method = RequestMethod.GET)
    public List<InvReMasterBank> getMasterBankInvestasiAll(){
		System.out.println("master");
        return (List<InvReMasterBank>) masterBankDao.getAllDataBankInvestasi();
    }
	
	@RequestMapping(value="/masterBank", method = RequestMethod.GET)
    public List<Bank> getMasterBankAll(){
		System.out.println("master");
        return (List<Bank>) masterBankDao.getAllDataBank();
    }

}
